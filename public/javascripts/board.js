var con = new WebSocket('ws:localhost/socketHandler', ['soap', 'xmpp']);

con.onerror = function (error) {
	console.log('WebSocket Error ' + error);
};

con.onmessage = function (e) {
	runEffect(e.data);
};


function runEffect(text) {
	$( "#shoutbox" ).show( "slide", 500, callback ).animate({height:70});
	setTimeout(function() {
		$("#shoutbox").html('<p>Streng dich an ' + text + '!<br>Du liegst hinten!</p>');
		$("#shoutbox p").fadeIn("slow");
	}, 1000);
};

function callback() {
    setTimeout(function() {
    	var options = {};
    	$("#shoutbox").hide("slide").animate({height:10});
    	$("#shoutbox p").hide();
    	
    }, 4000 );
};

var clientId = '320068315136-e7u2o7f935srlnqvhnnh04502ur7m4pc.apps.googleusercontent.com';
var scopes = 'https://www.googleapis.com/auth/plus.me';
var img;

function checkImg() {
	if(img) {
		$("#gameImgP1").attr("src", img);
	}
}

function checkAuth() {  
	gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
}

function handleAuthResult(authResult) {
	if(authResult.status.signed_in && !img && $("#splayer1").text() != "Batman") {
		gapi.client.load('plus', 'v1', function() {
			var request = gapi.client.plus.people.get({'userId' : 'me'});
			request.execute(function (resp) {
				img = resp.image.url;
				checkImg();
			});
		});
	}
}




