package controllers;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import models.HexxagonField;
import de.htwg.hexxagon.controller.impl.HexxagonController;
import de.htwg.hexxagon.model.IField;
import de.htwg.hexxagon.util.ConstantColors;
import play.libs.F.Callback0;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import util.MessageHandler;

public class Hexxagon extends Controller {

	// for development only
	private static boolean quickGameOver = false;

	private static HexxagonController controller;
	private static Point startOfMove = null;
	private static MessageHandler handler;
	private static int gameCommentsFreeze;

	public static Result gameNew(String p1, String p2) {
		if (StringUtils.isBlank(p1)) {
			p1 = "Batman";
		}
		if (StringUtils.isBlank(p2)) {
			p2 = "Joker";
		}
		controller = new HexxagonController();
		controller.setPlayers(p1, ConstantColors.COLOR_PLAYER1, p2,
				ConstantColors.COLOR_PLAYER2);
		gameCommentsFreeze = 0;

		// redirect to Hexxagon.gameRunning() to process game actions
		return redirect("/hexxagon");
	}

	public static Result gameRunning() {
		if (controller == null) {
			return Application.index();
		}
		// render page for hexxagon game
		return ok(views.html.hexxagon.hexxagon.render(getFieldList(null, null),
				controller));
	}

	public static Result gameRefresh(int xind, int yind) {
		// re-render hexxagon board inside page
		Point currentPoint = new Point(xind, yind);
		if (controller.isSelectable(currentPoint)) {
			// point selection not yet done
			startOfMove = currentPoint;
			List<Point> duplicateList = controller
					.getDuplicatePossibilities(currentPoint);
			List<Point> jumpList = controller
					.getJumpPossibilities(currentPoint);
			if (quickGameOver) {
				return ok(views.html.hexxagon.over.render(controller));
			} else {
				return ok(views.html.hexxagon.refresh.render(
						getFieldList(duplicateList, jumpList), controller));
			}
		} else {
			// point selection already done
			boolean success = false;
			if (startOfMove != null) {
				success = controller.doMove(startOfMove, currentPoint);
				if (success) {
					startOfMove = null;
					if (/*controller.gameOverByLeftPossibilities() ||*/ controller.gameOver()) {
						return ok(views.html.hexxagon.over.render(controller));
					}
				} else {
					return gameRefresh(startOfMove.x, startOfMove.y);
				}
			}
		}

		// trigger message box
		triggerGameComments();

		return ok(views.html.hexxagon.refresh.render(getFieldList(null, null),
				controller));
	}

	private static List<HexxagonField> getFieldList(List<Point> duplicateList,
			List<Point> jumpList) {
		// create list of non-black fields and convert indices from 0..12 to
		// 0..8
		// attention: swap indices in board array (board[j][i])
		IField[][] board = controller.getBoard();
		List<HexxagonField> fieldList = new ArrayList<HexxagonField>();
		int offset = 2;
		Color color;
		for (int i = offset; i < board.length - offset; i++) {
			for (int j = offset; j < board[i].length - offset; j++) {
				color = board[j][i].getColor();
				if (!board[j][i].getColor().equals(Color.BLACK)) {
					if (duplicateList != null && jumpList != null) {
						for (Point point : duplicateList) {
							if ((point.getX() == (i - offset))
									&& (point.getY() == (j - offset))) {
								color = Color.CYAN;
								break;
							}
						}
						for (Point point : jumpList) {
							if ((point.getX() == (i - offset))
									&& (point.getY() == (j - offset))) {
								color = Color.MAGENTA;
								break;
							}
						}
					}
					fieldList.add(new HexxagonField(i - offset, j - offset,
							color));
				}
			}
		}
		
		return fieldList;
	}

	private static void triggerGameComments() {
		int diff = Math.abs(controller.getPlayerScore(1)
				- controller.getPlayerScore(0));
		if (diff > 4 && gameCommentsFreeze == 0) {
			if (controller.getPlayerScore(0) < controller.getPlayerScore(1)) {
				System.out.println("first handler...");
				handler.sendMessage(controller.getPlayerName(0));
			} else if (controller.getPlayerScore(1) < controller
					.getPlayerScore(0)) {
				System.out.println("second handler...");
				handler.sendMessage(controller.getPlayerName(1));
			}
			gameCommentsFreeze = 5;
		}
		if (gameCommentsFreeze > 0) {
			gameCommentsFreeze--;
		}
	}

	public static WebSocket<String> socketHandler() {
		return new WebSocket<String>() {
			public void onReady(WebSocket.In<String> in,
					WebSocket.Out<String> out) {
				handler = new MessageHandler(out);
				in.onClose(new Callback0() {
					public void invoke() {
						System.out.println("Disconnected");
					}
				});
			}
		};
	}

}