package models;

import java.awt.Color;

public class HexxagonField {

	private int x;
	private int y;
	private String color;
	
	public HexxagonField(int x, int y, Color color) {
		this.x = x;
		this.y = y;
		this.color = "#" + Integer.toHexString(color.getRGB()).substring(2);
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public int getColorIndex() {
		if (this.color.equals(Color.WHITE)) {
			return 0;
		} else if (this.color.equals(Color.RED)) {
			return 1;
		} else if (this.color.equals(Color.BLUE)) {
			return 2;
		} else {
			return -1;
		}
	}
	
	@Override
	public String toString() {
		return "field[" + this.x + ", " + this.y + ", " + this.color + "]";
	}
}
