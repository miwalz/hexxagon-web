package util;

import play.mvc.WebSocket;

public class MessageHandler {

	WebSocket.Out<String> myOut;

	public MessageHandler(WebSocket.Out<String> out) {
		this.myOut = out;
	}

	public void sendMessage(String msg) {
		myOut.write(msg);
	}
}